package com.pamakids.umsocial;

import com.umeng.socialize.bean.SHARE_MEDIA;

/**
 * Created by kc2ong on 14-6-18.
 */
public class Platform {

    /** qq空间 */
    public static final String QZONE = "qzone";
    /** 微信朋友圈 */
    public static final String WEIXIN_CIRCLE = "wxtimeline";
    /** 微信好友 */
    public static final String WEIXIN = "wxsession";
    /** 腾讯qq好友 */
    public static final String QQ = "qq";
    /** 新浪微博 */
    public static final String SINA = "sina";
    /** 腾讯微博 */
    public static final String TENCENT = "tencent";
    /** 人人网 */
    public static final String RENREN = "renren";
    /** 豆瓣网 */
    public static final String DOUBAN = "douban";


    public static SHARE_MEDIA checkShareMedia(String platform){
        SHARE_MEDIA share_media = null;
        if ( platform.equals(Platform.SINA))
            share_media = SHARE_MEDIA.SINA;
        else if ( platform.equals(Platform.TENCENT))
            share_media = SHARE_MEDIA.TENCENT;
        else if( platform.equals(Platform.RENREN))
            share_media = SHARE_MEDIA.RENREN;
        else if( platform.equals(Platform.DOUBAN))
            share_media = SHARE_MEDIA.DOUBAN;
        else if(platform.equals(Platform.QQ))
            share_media = SHARE_MEDIA.QQ;
        else if( platform.equals(Platform.QZONE))
            share_media = SHARE_MEDIA.QZONE;
        else if(platform.equals(Platform.WEIXIN_CIRCLE))
            share_media = SHARE_MEDIA.WEIXIN_CIRCLE;
        else if(platform.equals(Platform.WEIXIN))
            share_media = SHARE_MEDIA.WEIXIN;
        return share_media;
    }

}

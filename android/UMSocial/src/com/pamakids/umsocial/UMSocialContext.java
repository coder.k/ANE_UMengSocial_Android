package com.pamakids.umsocial;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREInvalidObjectException;
import com.adobe.fre.FREObject;
import com.adobe.fre.FRETypeMismatchException;
import com.adobe.fre.FREWrongThreadException;
import com.pamakids.umsocial.DoOauthActivity;

/**
 * Created with IntelliJ IDEA.
 * User: mani
 * Date: 13-8-5
 * Time: PM5:12
 * To change this template use File | Settings | File Templates.
 */
public class UMSocialContext extends FREContext {

    public static final String TAG = "UMSocialContext";

    private static UMSocialContext _instance = null;

    public static UMSocialContext getInstance() {
        if(_instance == null)
            _instance = new UMSocialContext();
        return _instance;
    }

    public static void deleteInstance() {
        if(_instance != null)
        {
            _instance.dispose();
            _instance = null;
        }
    }

    @Override
    public void dispose(){
        Log.d(TAG, "Context disposed.");
    }

    @Override
    public Map<String, FREFunction> getFunctions(){
        Map<String, FREFunction> functions = new HashMap<String, FREFunction>();
        functions.put(init.TAG, new init());
        functions.put(initWeChat.TAG, new initWeChat());
        functions.put(initQQ.TAG, new initQQ());
        functions.put(share.TAG, new share());
        functions.put(login.TAG, new login());
        functions.put(cancelLogin.TAG, new cancelLogin());
        return functions;
    }

    /**友盟appkey*/
    private String umAppkey;
    private Boolean bugLog;

    /**微信appid*/
    private String wechatAppID;
    /**微信secret*/
    private String wechatAppSecret;
    /**微信分享中的链接地址*/
    private String wechatUrl;

    /**QQ平台的appid*/
    private String qqAppid;
    /**QQ平台appkey*/
    private String qqAppkey;
    /**QQ分享中的链接地址*/
    private String qqUrl;

    /**
     * 初始化友盟
     */
    private void init(String umappkey, Boolean buglog){
        umAppkey = umappkey;
        bugLog = buglog;
    }

    private void initWeChat(String wxappid, String appSecret, String weixinURL){
        wechatAppID = wxappid;
        wechatAppSecret = appSecret;
        wechatUrl = weixinURL;
    }

    private void initQQ(String appid, String appkey, String url){
        qqAppid = appid;
        qqAppkey = appkey;
        qqUrl = url;
    }

    private void login(String platform){

        Intent intent = new Intent(getActivity(), DoOauthActivity.class);
        intent.putExtra("method", DoOauthActivity.METHOD_LOGIN);
        intent.putExtra("platform", platform);
        intent.putExtra("umAppkey", umAppkey);

        if(platform.equals(Platform.QQ) || platform.equals(Platform.QZONE) || platform.equals(Platform.TENCENT))
        {
            intent.putExtra("qqAppid", qqAppid);
            intent.putExtra("qqAppkey", qqAppkey);
            intent.putExtra("qqUrl", qqUrl);
        }
        else if(platform.equals(Platform.WEIXIN) || platform.equals(Platform.WEIXIN_CIRCLE))
        {
            intent.putExtra("wechatAppID", wechatAppID);
            intent.putExtra("wechatAppSecret", wechatAppSecret);
            intent.putExtra("wechatUrl", wechatUrl);
        }

        getActivity().startActivity(intent);
//        getActivity().finish();
    }

    private void cancelLogin(String platform, String token){
        Intent intent = new Intent(getActivity(), DoOauthActivity.class);
        intent.putExtra("method", DoOauthActivity.METHOD_CANCEL);
        intent.putExtra("platform", platform);
        getActivity().startActivity(intent);
//        getActivity().finish();
    }

    private void share(String id, String shareText, String imageUrl, String title, String platform, String targetUrl){
        Intent intent = new Intent(getActivity(), DoOauthActivity.class);
        intent.putExtra("method", DoOauthActivity.METHOD_SHARE);
        intent.putExtra("platform", platform);

        if(platform.equals(Platform.QQ) || platform.equals(Platform.QZONE) || platform.equals(Platform.TENCENT))
        {
            intent.putExtra("qqAppid", qqAppid);
            intent.putExtra("qqAppkey", qqAppkey);
            intent.putExtra("qqUrl", qqUrl);
        }
        else if(platform.equals(Platform.WEIXIN) || platform.equals(Platform.WEIXIN_CIRCLE))
        {
            intent.putExtra("wechatAppID", wechatAppID);
            intent.putExtra("wechatAppSecret", wechatAppSecret);
            intent.putExtra("wechatUrl", wechatUrl);
        }

        intent.putExtra("id", id);
        intent.putExtra("shareText", shareText);
        intent.putExtra("imageUrl", imageUrl);
        intent.putExtra("title", title);
        intent.putExtra("targetUrl", targetUrl);

        getActivity().startActivity(intent);
//        getActivity().finish();
    }

    //==========================================================================================
    //================================↓↓↓FREFunctions↓↓↓==================================
    //==========================================================================================

    private class init implements FREFunction {

        public static final String TAG = "init";

        @Override
        public FREObject call(final FREContext frecContext, FREObject[] args){
            Log.d(TAG, "UMSocial init called");
            try {
                UMSocialContext context = (UMSocialContext)frecContext;
                String umappid = args[0].getAsString();
                Boolean bugLog = args[1].getAsBool();
                context.init(umappid, bugLog);
            } catch (FRETypeMismatchException e) {
                e.printStackTrace();
            } catch (FREInvalidObjectException e) {
                e.printStackTrace();
            } catch (FREWrongThreadException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class initWeChat implements FREFunction{
        public static final String TAG = "initWeChat";
        @Override
        public FREObject call(FREContext freContext, FREObject[] freObjects) {
            Log.d(TAG, "UMSocial initWeChat called");
            try {
                UMSocialContext context = (UMSocialContext)freContext;
                String wxappid = freObjects[0].getAsString();
                String appSecret = freObjects[1].getAsString();
                String weixinUrl = freObjects[2].getAsString();
                context.initWeChat(wxappid, appSecret, weixinUrl);
            }catch (FRETypeMismatchException e) {
                e.printStackTrace();
            } catch (FREInvalidObjectException e) {
                e.printStackTrace();
            } catch (FREWrongThreadException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class initQQ implements FREFunction{

        public static final String TAG = "initQQ";

        @Override
        public FREObject call(FREContext freContext, FREObject[] freObjects) {
            Log.d(TAG, "UMSocial initQQ called");

            try {
                UMSocialContext context = (UMSocialContext)freContext;
                String appid = freObjects[0].getAsString();
                String appkey = freObjects[1].getAsString();
                String url = freObjects[2].getAsString();
                context.initQQ(appid, appkey, url);
            }catch (FRETypeMismatchException e) {
                e.printStackTrace();
            } catch (FREInvalidObjectException e) {
                e.printStackTrace();
            } catch (FREWrongThreadException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class share implements FREFunction{

        public static final String TAG = "share";

        @Override
        public FREObject call(final FREContext freContext, FREObject[] args){

            Log.d(TAG, "UMSocial share called1");

            try {
                UMSocialContext context = (UMSocialContext)freContext;
                String id = args[0].getAsString();
                String shareText = args[1].getAsString();
                String imageUrl = args[2].getAsString();
                String title = args[3].getAsString();
                String platform = args[4].getAsString();
                String targetUrl = args[5].getAsString();
                context.share(id, shareText, imageUrl, title, platform, targetUrl);

            } catch (FRETypeMismatchException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                Log.d(TAG, e.getMessage());
            } catch (FREInvalidObjectException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                Log.d(TAG, e.getMessage());
            } catch (FREWrongThreadException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                Log.d(TAG, e.getMessage());
            }
            return null;
        }
    }

    public class login implements FREFunction {

        public static final String TAG = "login";

        @Override
        public FREObject call(FREContext freContext, FREObject[] freObjects) {

            Log.d(TAG, "UMSocial login called");
            try {
                UMSocialContext context = (UMSocialContext)freContext;
                String platform = freObjects[0].getAsString();
                context.login(platform);
            } catch (FRETypeMismatchException e) {
                e.printStackTrace();
            } catch (FREInvalidObjectException e) {
                e.printStackTrace();
            } catch (FREWrongThreadException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class cancelLogin implements FREFunction {

        public static final String TAG = "cancelLogin";

        @Override
        public FREObject call(FREContext freContext, FREObject[] freObjects) {

            Log.d(TAG, "UMSocial cancleLogin called");

            try {
                UMSocialContext context = (UMSocialContext)freContext;
                String platform = freObjects[0].getAsString();
                String token = freObjects[1].getAsString();
                context.cancelLogin(platform, token);
            } catch (FRETypeMismatchException e) {
                e.printStackTrace();
            } catch (FREInvalidObjectException e) {
                e.printStackTrace();
            } catch (FREWrongThreadException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
